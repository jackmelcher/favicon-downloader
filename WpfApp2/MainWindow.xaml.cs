﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Net;
using System.IO;


namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>


    public partial class MainWindow : Window
    {
        string urls;
        string[] domainNames;
        WebClient wc;
        string host;
        string dir;

        public MainWindow()
        {
            InitializeComponent();

            makeSizeLists();
            wc = new WebClient();
        }

        // Read contents of the TextBox containing the URLs and split into array using '\n' delimiter.

        // Get the root domain url using built-in functions and/or with regex.
        // Loop through array and save 32x32 .png of the favicon retreived from favicon kit API to local directory where the App exists.

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dir = textBox_subfolder.Text;
                if (dir != "")
                {
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    dir = dir + "\\";
                }

                textBlock_progress.Text = "Downloading...";

                urls = textBox_url.Text;

                domainNames = urls.Split('\n');


                foreach (string item in domainNames)
                {
                    host = new Uri(item).Host;
                    wc.DownloadFile("https://api.faviconkit.com/" + host + "/" + comboBox_size.SelectedValue.ToString(), dir + host + ".png"); // Add subfolder creation and saving.
                    Console.WriteLine("Accessed: " + "https://api.faviconkit.com/" + host + "/" + comboBox_size.SelectedValue.ToString());
                    Console.WriteLine(host + ".png");
                }

                textBlock_progress.Text = "Downloading Complete!";
            }
            catch (Exception ex)
            {
                textBlock_progress.Text = "Please insert valid URLs";
                Console.WriteLine("Exception: " + ex);
            }
        }

        private void makeSizeLists()
        {
            List<string> sizes;

            sizes = new List<string>();
            sizes.Add("15");
            sizes.Add("31");
            sizes.Add("56");
            sizes.Add("71");
            sizes.Add("113");
            sizes.Add("119");
            sizes.Add("143");
            comboBox_size.ItemsSource = sizes;
            comboBox_size.SelectedIndex = 1;
        }
    }    
}
